import { createStore } from 'vuex'
import { mount } from '@vue/test-utils'
import UserListEntry from '@/components/UserListEntry.vue'
import UserDetails from '@/components/UserDetails.vue'
import { User } from '@/types/User'

const store = createStore({
  state() {
    return {
      selectedUser: undefined,
    }
  }
})

describe('UserListEntry.vue', () => {
  it('renders one user list entry correctly', () => {
    const user = {
      contact: {
        email: 'andrew.andersson@gmail.com',
      },
      picture: {
        thumbnail: 'https://randomuser.me/api/portraits/thumb/men/58.jpg',
      },
      name: {
        first: 'Andrew',
        last: 'Andersson',
      }
    } as User;

    const wrapper = mount(UserListEntry, {
      props: { user },
      global: {
        plugins: [store],
      }
    })

    const expectedHtml = `
      <div class="user-list-entry">
        <span>
          <img src="https://randomuser.me/api/portraits/thumb/men/58.jpg" alt="user thumbnail">
        </span>
        <span>
          <h3>Andrew Andersson</h3>
          <p>andrew.andersson@gmail.com</p>
        </span>
      </div>
    `.split('\n').join('').split("  ").join("");

    expect(wrapper.html()).toEqual(expectedHtml);
  })
})

describe('UserDetails.vue', () => {
  it('renders the details of one user correctly', () => {
    const user = {
      id: 'FN 18088310701',
      location: {
        city: 'Stockholm',
        country: 'Sweden',
        state: 'Stockholm County',
        postcode: 12345,
      },
      contact: {
        email: 'andrew.andersson@gmail.com',
        phone: '92183691',
        cell: '12349723',
      },
      picture: {
        large: 'https://randomuser.me/api/portraits/men/58.jpg',
      },
      name: {
        first: 'Andrew',
        last: 'Andersson',
      }
    } as User;

    const wrapper = mount(UserDetails, {
      props: { user },
    })

    const expectedHtml = `
      <section id="user-details">
        <div id="user-details-inner-container">
          <button type="button" id="user-details-back-button" name="back-button"> Back </button>
          <span>
            <img src="https://randomuser.me/api/portraits/men/58.jpg" alt="user profile picture">
          </span>
          <span>
            <div class="user-details-section">
              <h2>Andrew Andersson</h2>
              <p><strong>ID: FN 18088310701</strong></p>
            </div>
            <div class="user-details-section">
              <p>Located in Stockholm, Sweden</p>
              <p>Stockholm County 12345</p>
              <p>andrew.andersson@gmail.com</p>
              <p>Phone number: 92183691</p>
              <p>Mobile number: 12349723</p>
            </div>
          </span>
        </div>
      </section>
    `.split('\n').join('').split("  ").join("");

    expect(wrapper.html().split('\n').join('').split("  ").join("")).toEqual(expectedHtml);
  })
})
