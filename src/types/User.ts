import { Gender } from './Gender'
import { Location } from './Location'
import { Name } from './Name'
import { Picture } from './Picture'
import { Contact } from './Contact'

export type User = {
  id: string,
  gender: Gender,
  location: Location,
  name: Name,
  picture: Picture,
  contact: Contact,
}
