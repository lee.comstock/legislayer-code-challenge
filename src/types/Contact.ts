export type Contact = {
  phone: string,
  cell: string,
  email: string,
}
