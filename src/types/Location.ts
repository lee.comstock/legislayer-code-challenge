export type Location = {
  city: string,
  country: string,
  postcode: number,
  state: string,
}
