export type Name = {
  first: string,
  last: string,
  title: string,
}
