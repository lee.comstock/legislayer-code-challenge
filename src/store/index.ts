import { createStore } from 'vuex'
import { User } from '../types/User'
import { Gender } from '../types/Gender'
import { Location } from '../types/Location'
import { Name } from '../types/Name'
import { Picture } from '../types/Picture'
import { Contact } from '../types/Contact'
import axios from 'axios'

export default createStore({
  state: {
    users: [] as User[],
    selectedUser: undefined as User | undefined,
    requestInProgress: false,
  },
  mutations: {
    addUsers(state, { users }) {
      localStorage.setItem('users', JSON.stringify(users));
      for (let user of users) {
        state.users.push({
          id: user.id.value ? user.id.name + ' ' + user.id.value : null,
          gender: (user.gender == 'male') ? Gender.male : Gender.female,
          location: user.location as Location,
          name: user.name as Name,
          picture: user.picture as Picture,
          contact: {
            phone: user.phone,
            cell: user.cell,
            email: user.email,
          } as Contact,
        } as User);
      }
    },
    setSelectedUser(state, { user }) {
      localStorage.setItem('selectedUser', JSON.stringify(user));
      state.selectedUser = user;
    },
    setRequestInProgress(state, { value }) {
      state.requestInProgress = value;
    },
  },
  actions: {
    init(context) {
      if (localStorage.getItem('users')) {
        context.commit('addUsers', {
          users: JSON.parse(localStorage.getItem('users') as string) 
        });
        if (localStorage.getItem('selectedUser')) {
          context.commit('setSelectedUser', {
            user: JSON.parse(localStorage.getItem('selectedUser') as string)
          });
        }
      } else {
        context.dispatch('fetchUsers', { count: 25 });
      }
    },
    fetchUsers(context, { count }) {
      if (context.state.requestInProgress) return;
      context.commit('setRequestInProgress', { value: true });
      axios({
        method: 'get',
        url: 'https://randomuser.me/api/?inc=gender,name,location,email,phone,cell,id,picture&results=' + count,
        responseType: 'json',
      }).then((response) => {
        console.log(response.data);
        context.commit('addUsers', { users: response.data.results });
        context.commit('setRequestInProgress', { value: false });
      }).catch((error) => {
        console.error(error);
      });
    }
  },
  modules: {
  }
});
